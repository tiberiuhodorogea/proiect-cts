package jUnitTests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import SharedClasses.Communication.*;
import SharedClasses.Communication.Exceptions.KeyNotMappedException;
import SharedClasses.Objects.Car;
import SharedClasses.Objects.Credentials;

public class RequestTesting {

	Request req;
	@Before
	public void setUp() throws Exception {
	}

	
	@Test
	public void creationTest() {
		try {
			req = new SharedClasses.Communication.RequestFactory().create(RequestedAction.CHECK_ACCESS,
					new Credentials("test@mail.com","1234"));
			String expected = "{\"requestedAction\":\"CHECK_ACCESS\",\"jsonData\":\"{\\\"userMail\\\":\\\"test@mail.com\\\",\\\"password\\\":\\\"1234\\\"}\"}";
			System.out.println(expected);
			System.out.println(req.toJson());
			assertEquals(expected, req.toJson());
			
		} catch (KeyNotMappedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void exceptionTestOnNotImplementedPair(){
		try {
			req = new SharedClasses.Communication.RequestFactory().create(RequestedAction.TEST_NOT_MAPPED,
					new Car());
			Object o = req.deserializeData(); // should throw exception
			fail();
		} catch (KeyNotMappedException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() throws Exception {
	}


}
