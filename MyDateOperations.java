package jUnitTests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import SharedClasses.Utils.MyDate;

public class MyDateOperations {

	MyDate testDate;
	
	@Before
	public void setUp() throws Exception {
		testDate = new MyDate(23,6,2010);
	}

	@After
	public void tearDown() throws Exception {
		
		
	}

	@Test
	public void dateDifferenceTestOne() {
		MyDate localDate = new MyDate(10,5,2010);
		
		String expectedDifference = "13-01-00";
		
		assertEquals(expectedDifference, testDate.minus(localDate).toString());
		
		System.out.println(testDate.minus(localDate).toString());
	}
	
	@Test
	public void dateDifferenceTestTwo() {
		MyDate localDate = new MyDate(25,4,2009);
		
		String expectedDifference = "28-01-01";
		
		assertEquals(expectedDifference, testDate.minus(localDate).toString());
		
		System.out.println(testDate.minus(localDate).toString());
	}
	
	
	@Test
	public void dateInFutureMethodTest(){
		testDate= new MyDate(1,1,2017);
		if(testDate.isInFuture())
			;//ok
		else
			fail();
		
		
	}

}
