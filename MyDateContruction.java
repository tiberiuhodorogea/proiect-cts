package jUnitTests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import SharedClasses.Utils.MyDate;

public class MyDateContruction {
	
	MyDate testDate;
	

	@Before
	public void setUp() throws Exception {
		testDate = new MyDate(1,1,2000);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void InvalidDayTest() {
		//testing for day>31
		try{
			MyDate date = new MyDate(32,10,2001);
			fail("Created date with day = 32");
		}catch(Exception e){
		}
	}
	
	@Test
	public void InvalidMonthTestSetter(){
		
		try{
			testDate.setMonth(13);
			fail("set month 13");
		}
		catch(Exception e){
			//it's ok
		}
	}
	
	@Test	
	public void NegativeYearTest(){
		try{
			testDate.setYear(-200);
			fail("Set negative year");
		}
		catch(Exception e){
			//it's ok
		}
	}

}
