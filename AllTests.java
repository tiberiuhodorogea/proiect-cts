package jUnitTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MyDateContruction.class, MyDateOperations.class, RequestTesting.class })
public class AllTests {

}
